# Tribal Wars Tools: Attack Planner

Python desktop application for generating attack plans for the whole tribe, as well as for each player separately.
Currently only Polish language is supported.

If you are looking for a previous version that used an external GraphQL API (no longer working), please check the `external-api` branch. However, the version in that branch does not have all the features.

This version of the application needs to be manually adjusted to your world settings before using it. It is still functional, but not very efficient.

## How to customize your world settings?

`data.py`

- line `28`: Tribal Wars base URL

`gui.py`

- line `113`: World name label
- line `352`: World key
- line `358`: Max snob distance

`models.py`

- line `334`: Units speeds

## How to build executable?

```
pyinstaller .\attack_planner.py --onedir --noconsole --hidden-import babel.numbers
```
