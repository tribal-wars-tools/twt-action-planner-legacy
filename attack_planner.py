import gui
import models


def main():
    create_headers()

    main_window = gui.MainWindow("Automatyczny planer ataków")
    main_window.run()


def create_headers():
    models.Header("Sygnatura czasowa", "OriginsDataTab", usable=False)
    models.Header("Nick", "OriginsDataTab", usable=True)
    models.Header("Wioska docelowa", "TargetsDataTab", usable=True)

    models.TroopsType("Off pełny", "ram")
    models.TroopsType("Off 3/4", "ram")
    models.TroopsType("Off 1/2", "ram")
    models.TroopsType("Off 1/3", "ram")
    models.TroopsType("Off burzący", "ram")
    models.TroopsType("Off otwierający", "ram")
    models.TroopsType("Offoszlachta", "snob")
    models.TroopsType("Katapulty z wioski deff", "catapult")
    models.TroopsType("1x Szlachcic", "snob")
    models.TroopsType("2x Szlachcic", "snob")
    models.TroopsType("3x Szlachcic", "snob")
    models.TroopsType("4x Szlachcic", "snob")
    models.TroopsType("Deffoszlachta", "snob")
    models.TroopsType("FEJK taran", "ram")

    for troops_type in models.TroopsType.obj_list:
        models.Header(
            troops_type.name, "OriginsDataTab", header_type=troops_type, usable=True
        )
        models.Header(
            troops_type.name, "TargetsDataTab", header_type=troops_type, usable=True
        )


if __name__ == "__main__":
    main()
