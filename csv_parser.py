import csv
import itertools
import models
import utils


def parse(path, tab_class_name, has_header=True):
    N_LINES = 10

    with open(path, encoding="utf-8") as csvfile:
        try:
            sample = "".join(itertools.islice(csvfile, N_LINES)).strip()
        except UnicodeDecodeError:
            return False
        csvfile.seek(0)

        dialect = csv.Sniffer().sniff(sample, [",", ";", ":", " ", "\t"])
        try:
            reader = csv.reader(csvfile, dialect)
        except UnicodeDecodeError:
            return False

        filtered_headers = [
            header
            for header in models.Header.obj_list
            if header.tab_class_name == tab_class_name
            and header.selected
            and header.usable
        ]

        """
        Sortowanie, aby najpierw były odczytywane kolumny bez przypisanego typu wojsk, tj. gracze i wioski.
        Bez nich nie będzie możliwości późniejszego przypisania koordynatów - wszędzie zera.
        """
        filtered_headers = sorted(
            filtered_headers,
            key=lambda header: header.header_type in models.TroopsType.obj_list,
        )

        """ Pomijanie wiersza nagłówkowego, jeśli występuje """
        if has_header:
            reader = itertools.islice(reader, 1, None)

        for row in reader:
            player = None
            target_village = None

            for header in filtered_headers:
                try:
                    if header.header_type in models.TroopsType.obj_list:
                        if tab_class_name == "OriginsDataTab":
                            if player is None:
                                return False
                            else:
                                text = row[header.index].strip()
                                for coords in utils.get_coords_from_text(text):
                                    origin_village = models.Village.get(coords)
                                    if not origin_village:
                                        origin_village = models.Village(coords)
                                    models.Origin(
                                        player, origin_village, header.header_type
                                    )
                        elif tab_class_name == "TargetsDataTab":
                            if target_village is None:
                                return False
                            else:
                                try:
                                    amount = int(row[header.index])
                                    if amount > 0:
                                        target = models.Target.get(
                                            target_village, header.header_type
                                        )
                                        if target:
                                            target.amount += amount
                                        else:
                                            models.Target(
                                                target_village,
                                                amount,
                                                header.header_type,
                                            )
                                except ValueError:
                                    return False
                    else:
                        text = row[header.index].strip()
                        coords = utils.get_coords_from_text(text)
                        if coords:
                            if len(coords) > 1:
                                return False
                            else:
                                target_village = models.Village.get(coords[0])
                                if not target_village:
                                    target_village = models.Village(coords[0])
                        else:
                            nickname = row[header.index].strip()
                            player = models.Player(nickname)
                except IndexError:
                    return False
        else:
            return True
