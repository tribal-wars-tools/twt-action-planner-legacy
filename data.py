import csv

from typing import TypedDict
from urllib.parse import urlparse, urljoin, unquote_plus

import requests
import tldextract


class VillageDict(TypedDict):
    id: int
    name: str
    x: int
    y: int
    player_id: int
    points: int


class VillageData:
    def __init__(self, world_key: str):
        self.world_key = world_key
        self.villages: list[VillageDict] = []

    def get(self) -> list[VillageDict]:
        if self.villages:
            return self.villages

        base_url: str = "https://www.plemiona.pl/"

        extracted = tldextract.extract(base_url)
        parsed = urlparse(base_url)
        replaced = parsed._replace(
            netloc=f"{self.world_key}.{extracted.domain}.{extracted.suffix}"
        )

        file_url = urljoin(replaced.geturl(), "map/village.txt")

        response = requests.get(file_url, stream=True)

        with response as r:
            reader = csv.reader(r.text.splitlines())
            for line in reader:
                self.villages.append(
                    VillageDict(
                        id=int(line[0]),
                        name=unquote_plus(line[1]),
                        x=int(line[2]),
                        y=int(line[3]),
                        player_id=int(line[4]),
                        points=int(line[5]),
                    )
                )

        return self.villages
