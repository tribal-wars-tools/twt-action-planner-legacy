import jinja2
import models
import utils


def generate(world_key: str):
    timeframes_headers = models.Timeframe.get_headers()

    models.Message.clear_list()

    file_loader = jinja2.FileSystemLoader("templates")
    env = jinja2.Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    env.filters["format_datetime"] = utils.format_datetime
    template = env.get_template("individual_message.jinja2")

    headers = [
        header
        for header in models.Header.obj_list
        if header.tab_class_name == "OriginsDataTab"
        and header.usable == True
        and header.selected == True
        and header.header_type is not None
    ]

    for player in models.Player.obj_list:
        player_timeplans = [
            timeplan
            for timeplan in models.Timeplan.obj_list
            if timeplan.origin.player == player
        ]

        if not player_timeplans:
            continue

        timeplans_sorted = sorted(player_timeplans, key=lambda timeplan: timeplan.start)

        output = template.render(
            world_key=world_key,
            date=timeframes_headers.dates,
            weekday=timeframes_headers.weekdays,
            player=player,
            headers=headers,
            timeplans=timeplans_sorted,
        )
        models.Message(player, output)
