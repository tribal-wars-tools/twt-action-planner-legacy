import datetime
from dataclasses import dataclass

import babel

import constants
import data
import utils


def clear_lists():
    Player.clear_list()
    Message.clear_list()
    Village.clear_list()
    Origin.clear_list()
    Target.clear_list()
    Timeplan.clear_list()


@dataclass
class CalculatedDatetimes:
    start_datetime: datetime.datetime
    end_datetime: datetime.datetime | None


class Player:
    obj_list = []

    def __init__(self, nickname):
        self.nickname = nickname

        self.obj_list.append(self)

    @classmethod
    def clear_list(cls):
        cls.obj_list.clear()

    @classmethod
    def get_player_by_nickname(cls, nickname):
        for player in cls.obj_list:
            if player.nickname == nickname:
                return player

        return None

    def __eq__(self, other):
        return self.nickname == other.nickname


class Message:
    obj_list = []

    def __init__(self, player, message):
        self.player = player
        self.message = message

        self.obj_list.append(self)

    @classmethod
    def clear_list(cls):
        cls.obj_list.clear()

    @classmethod
    def get_msg_by_nickname(cls, nickname):
        for message in cls.obj_list:
            if message.player.nickname == nickname:
                return message

        return None

    def __str__(self):
        return self.message


class Village:
    id = None

    __obj_list = {}

    def __init__(self, coords):
        self.coords = coords
        self.__obj_list[coords] = self

    @classmethod
    def clear_list(cls):
        cls.__obj_list.clear()

    @classmethod
    def get(cls, coords):
        if coords in cls.__obj_list:
            return cls.__obj_list[coords]

        return None

    @classmethod
    def get_all_coords(cls):
        return [village for village in cls.__obj_list]

    def __eq__(self, other):
        return self.coords == other.coords


class Origin:
    obj_list = []

    def __init__(self, player, village, troops_type):
        self.player = player
        self.village = village
        self.troops_type = troops_type

        self.obj_list.append(self)

    @classmethod
    def clear_list(cls):
        cls.obj_list.clear()

    @classmethod
    def get(cls, coords, troops_type):
        for origin in cls.obj_list:
            if origin.village.coords == coords and origin.troops_type == troops_type:
                return origin

        return None


class Target:
    obj_list = []

    def __init__(self, village, amount, troops_type):
        self.village = village

        self.amount = amount
        self.troops_type = troops_type

        # self.__origins_list = []

        self.obj_list.append(self)

    @classmethod
    def clear_list(cls):
        cls.obj_list.clear()

    @classmethod
    def get(cls, village, troops_type):
        for target in cls.obj_list:
            if target.village == village and target.troops_type == troops_type:
                return target

        return None

    # @property
    # def origins_list(self):
    #     return self.__origins_list

    # def add_origin(self, origin):
    #     self.__origins_list.append(origin)
    #     origin.available = False

    # def get_origins_amount(self):
    #     return len(self.__origins_list)

    # def is_target_filled(self):
    #     return self.amount == len(self.__origins_list)


class Header:
    obj_list = []

    def __init__(
        self,
        name,
        tab_class_name,
        header_type=None,
        usable=False,
        selected=False,
        index=None,
    ):
        self.name = name
        self.tab_class_name = tab_class_name
        self.header_type = header_type
        self.usable = usable
        self.selected = selected

        self.__index = index

        self.obj_list.append(self)

    @classmethod
    def clear_list(cls):
        cls.obj_list.clear()

    @classmethod
    def get(cls, tab_class_name, header_type):
        for header in cls.obj_list:
            if (
                header.tab_class_name == tab_class_name
                and header.header_type == header_type
            ):
                return header

        return None

    @classmethod
    def get_header_by_name(cls, tab_class_name, name):
        for header in cls.obj_list:
            if header.tab_class_name == tab_class_name and header.name == name:
                return header

        return None

    @property
    def index(self):
        return self.__index

    @index.setter
    def index(self, index):
        if self.selected and (isinstance(index, int) or (index is None)):
            self.__index = index
            return True

        return False


@dataclass
class TimeframesHeaders:
    dates: str
    weekdays: str


class Timeframe:
    obj_list = {}

    def __init__(self, start, end, timeframe_type):
        self.start = start
        self.end = end
        self.timeframe_type = timeframe_type

        self.obj_list[timeframe_type] = self

    @classmethod
    def clear_list(cls):
        cls.obj_list.clear()

    @classmethod
    def get_headers(cls):
        min_start_date = min(
            timeframe.start for timeframe in cls.obj_list.values()
        ).date()
        max_end_date = max(timeframe.end for timeframe in cls.obj_list.values()).date()

        tzinfo = babel.dates.get_timezone_name(
            locale=constants.LOCALE, return_zone=True
        )

        if min_start_date == max_end_date:
            dates_string = babel.dates.format_datetime(
                min_start_date, "dd MMMM", tzinfo=tzinfo, locale=constants.LOCALE
            )
            weekdays_string = babel.dates.format_datetime(
                min_start_date, "EEEE", tzinfo=tzinfo, locale=constants.LOCALE
            )
        else:
            dates_string = (
                f"{babel.dates.format_datetime(min_start_date, 'dd', tzinfo=tzinfo, locale=constants.LOCALE)}"
                "-"
                f"{babel.dates.format_datetime(max_end_date, 'dd MMMM', tzinfo=tzinfo, locale=constants.LOCALE)}"
            )
            weekdays_string = (
                f"{babel.dates.format_datetime(min_start_date, 'EEEE', tzinfo=tzinfo, locale=constants.LOCALE)}"
                "-"
                f"{babel.dates.format_datetime(max_end_date, 'EEEE', tzinfo=tzinfo, locale=constants.LOCALE)}"
            )

        return TimeframesHeaders(dates_string, weekdays_string)


class Timeplan:
    obj_list = []

    def __init__(self, origin, target, start, end):
        self.origin = origin
        self.target = target
        self.start = start
        self.end = end

        self.obj_list.append(self)

    @classmethod
    def clear_list(cls):
        cls.obj_list.clear()

    @classmethod
    def is_target_filled(cls, target):
        target_timeplans = [
            timeplan for timeplan in cls.obj_list if timeplan.target == target
        ]
        target_timeplans_amount = len(target_timeplans)

        if target.amount > target_timeplans_amount:
            return False

        return True

    @classmethod
    def is_origin_available(cls, origin):
        for timeplan in cls.obj_list:
            if timeplan.origin == origin:
                return False
        return True

    @classmethod
    def remove(cls, timeplan):
        cls.obj_list.remove(timeplan)

    def calculate(self, world_key) -> CalculatedDatetimes:
        troops_type = self.target.troops_type
        timeframe = Timeframe.obj_list[troops_type]

        if not timeframe:
            return

        unit_name = self.target.troops_type.unit

        if not unit_name:
            return

        villages_distance = utils.get_coords_distance(
            self.origin.village.coords, self.target.village.coords
        )

        # worlds_data = data.Worlds()
        # unit_speed = worlds_data.get_unit_speed(world_key, unit_name)

        units_speed = {"ram": 30, "snob": 35, "catapult": 30}

        if not unit_name in units_speed:
            return

        unit_speed = round(float(units_speed[unit_name]), 2)

        travel_time = villages_distance * datetime.timedelta(minutes=unit_speed)

        if timeframe.start == timeframe.end:
            self.start = timeframe.start - travel_time
        else:
            self.start = timeframe.start - travel_time
            self.end = timeframe.end - travel_time

        return CalculatedDatetimes(self.start, self.end if self.end else None)


class TroopsType:
    obj_list = []

    def __init__(self, name, unit):
        self.name = name
        self.unit = unit
        self.obj_list.append(self)

    def __str__(self):
        return self.name
