markupsafe==2.0.1
tk==0.1.0
Babel==2.9.0
tkcalendar==1.6.1
Jinja2==2.11.2
requests>=2.27.1
black>=22.1.0
tldextract>=3.2.1
