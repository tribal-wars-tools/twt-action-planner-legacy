from datetime import datetime, time
from itertools import zip_longest

import babel

import constants
import models
import utils
from data import VillageData


def zip_discard_gen(*iterables, sentinel=object()):
    return (
        tuple([entry for entry in iterable if entry is not sentinel])
        for iterable in zip_longest(*iterables, fillvalue=sentinel)
    )


def distribute(
    snob_max_distance,
    world_key,
    time_window_start: time,
    time_window_end: time,
    village_data_obj: VillageData,
    dispose_end_time_passed: bool,
):
    troops_types = models.TroopsType.obj_list

    village_data = village_data_obj.get()

    tz = babel.dates.get_timezone_name(locale=constants.LOCALE, return_zone=True)
    tzinfo = babel.dates.get_timezone(tz)
    current_datetime = datetime.now(tzinfo)

    origins = {}
    for troops_type in troops_types:
        origins[troops_type] = [
            origin
            for origin in models.Origin.obj_list
            if origin.troops_type == troops_type
        ]

    for target in models.Target.obj_list:
        if models.Timeplan.is_target_filled(target):
            continue

        if target.village.id is None:
            coords_axides = utils.split_coords_into_axides(target.village.coords)
            target.village.id = next(
                (
                    village["id"]
                    for village in village_data
                    if village["x"] == coords_axides[0]
                    and village["y"] == coords_axides[1]
                ),
                None,
            )

        if origins[target.troops_type]:
            origins_available = [
                origin
                for origin in origins[target.troops_type]
                if models.Timeplan.is_origin_available(origin)
            ]

            if origins_available:
                origins_sorted = sorted(
                    origins_available,
                    key=lambda origin: utils.get_coords_distance(
                        origin.village.coords, target.village.coords
                    ),
                )

                if target.troops_type.unit == "snob":
                    origins_sorted = [
                        origin
                        for origin in origins_sorted
                        if utils.get_coords_distance(
                            origin.village.coords, target.village.coords
                        )
                        <= snob_max_distance
                    ]

                    count = 0

                    for origin in origins_sorted:
                        if origin.village.id is None:
                            coords_axides = utils.split_coords_into_axides(
                                origin.village.coords
                            )
                            origin.village.id = next(
                                (
                                    village["id"]
                                    for village in village_data
                                    if village["x"] == coords_axides[0]
                                    and village["y"] == coords_axides[1]
                                ),
                                None,
                            )

                        timeplan = models.Timeplan(origin, target, None, None)
                        calculated_datetimes = timeplan.calculate(world_key)

                        if not dispose_end_time_passed and (
                            (
                                calculated_datetimes.end_datetime is not None
                                and (
                                    calculated_datetimes.end_datetime < current_datetime
                                )
                            )
                            or (
                                calculated_datetimes.end_datetime is None
                                and (
                                    calculated_datetimes.start_datetime
                                    < current_datetime
                                )
                            )
                        ):
                            models.Timeplan.remove(timeplan)
                            continue

                        count += 1

                        if count >= target.amount:
                            break

                else:
                    player_origins_sorted = []
                    for player in models.Player.obj_list:
                        player_origins_sorted.append(
                            [
                                origin
                                for origin in origins_sorted
                                if origin.player == player
                            ]
                        )

                    origins_cycled = zip_discard_gen(*player_origins_sorted)
                    origins_cycled = sum(origins_cycled, ())

                    count = 0

                    for origin in origins_cycled:
                        if origin.village.id is None:
                            coords_axides = utils.split_coords_into_axides(
                                origin.village.coords
                            )
                            origin.village.id = next(
                                (
                                    village["id"]
                                    for village in village_data
                                    if village["x"] == coords_axides[0]
                                    and village["y"] == coords_axides[1]
                                ),
                                None,
                            )

                        timeplan = models.Timeplan(origin, target, None, None)
                        calculated_datetimes = timeplan.calculate(world_key)
                        start_time = calculated_datetimes.start_datetime.time()

                        if (time_window_end < start_time < time_window_start) or (
                            not dispose_end_time_passed
                            and (
                                (
                                    calculated_datetimes.end_datetime is not None
                                    and (
                                        calculated_datetimes.end_datetime
                                        < current_datetime
                                    )
                                )
                                or (
                                    calculated_datetimes.end_datetime is None
                                    and (
                                        calculated_datetimes.start_datetime
                                        < current_datetime
                                    )
                                )
                            )
                        ):
                            models.Timeplan.remove(timeplan)
                            continue

                        count += 1

                        if count >= target.amount:
                            break
                    else:
                        for origin in origins_cycled:
                            if count >= target.amount:
                                break

                            if not models.Timeplan.is_origin_available(origin):
                                continue

                            if origin.village.id is None:
                                coords_axides = utils.split_coords_into_axides(
                                    origin.village.coords
                                )
                                origin.village.id = next(
                                    (
                                        village["id"]
                                        for village in village_data
                                        if village["x"] == coords_axides[0]
                                        and village["y"] == coords_axides[1]
                                    ),
                                    None,
                                )

                            timeplan = models.Timeplan(origin, target, None, None)
                            calculated_datetimes = timeplan.calculate(world_key)

                            if not dispose_end_time_passed and (
                                (
                                    calculated_datetimes.end_datetime is not None
                                    and (
                                        calculated_datetimes.end_datetime
                                        < current_datetime
                                    )
                                )
                                or (
                                    calculated_datetimes.end_datetime is None
                                    and (
                                        calculated_datetimes.start_datetime
                                        < current_datetime
                                    )
                                )
                            ):
                                models.Timeplan.remove(timeplan)
                                continue

                            count += 1
