from enum import IntEnum

import re
import math
import babel
import constants


class Axides(IntEnum):
    X_AXIS = 0
    Y_AXIS = 1


def get_coords_from_text(text):
    return re.findall(r"(\d{3}\|\d{3})", text, re.MULTILINE)


def split_coords_into_axides(coords):
    try:
        return list(map(int, coords.split("|")))
    except ValueError:
        return None


def get_coords_distance(origin, destination):
    origin_coords = split_coords_into_axides(origin)
    destination_coords = split_coords_into_axides(destination)

    if origin_coords and destination_coords:
        x_diff = abs(destination_coords[Axides.X_AXIS] - origin_coords[Axides.X_AXIS])
        y_diff = abs(destination_coords[Axides.Y_AXIS] - origin_coords[Axides.Y_AXIS])
        distance = math.sqrt(pow(x_diff, 2) + pow(y_diff, 2))

        return round(distance, 2)

    return None


def format_datetime(dt):
    tzinfo = babel.dates.get_timezone_name(locale=constants.LOCALE, return_zone=True)
    return babel.dates.format_datetime(
        dt, "dd MMMM yyyy, HH:mm:ss", tzinfo=tzinfo, locale=constants.LOCALE
    )
